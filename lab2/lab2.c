#include <stdio.h>

int main()
{
    int array [11] = {23, 50, 32, -63, -55, -5, 15, -84, -5, 7, -152};

    int less_sum = 0;
    int great_sum = 0;

    int M;
    printf("Enter M: ");
    scanf("%d", &M);

    for (int i = 0; i < 11; i++)
    {
        if (array[i] < M)
            less_sum += array[i];
        else if (array[i] > M)
            great_sum += array[i];
    }

    printf("Sum greater M = %d \n", great_sum);
    printf("Sum less M = %d \n", less_sum);

    return 0;
}
