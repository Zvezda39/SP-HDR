.data
array:
    .long 23, 50, 32, -63, -55, -5, 15, -84, -5, 7, -152
array_end:

format_output_less:
    .string "Sum less M = %d \n"

format_output_greater:
    .string "Sum greater M = %d\n"

M:
    .long 0

form_input:
    .string "Enter M: "

format_input_num:
    .string "%d"


.text
.global main

main:                           
    pushl $form_input           
    call printf                 
    addl $4, %esp               

    pushl $M                    
    pushl $format_input_num     
    call scanf                  
    addl $8, %esp               

    movl $M, %eax               
    movl (%eax), %eax           

    
    xorl %ebx, %ebx            
    xorl %ecx, %ecx             
    movl $array, %esi           

check_loop:                   
    movl (%esi), %edx           
    cmpl %eax, %edx            
    jg greater                 
    jl less                     
    jmp next                    

greater:
    addl %edx, %ebx            
    jmp next                    

less:
    addl %edx, %ecx             

next:
    addl $4, %esi               
    cmpl $array_end, %esi       
    jne check_loop             

    pushl %ecx                  

    pushl %ebx                  
    pushl $format_output_greater
    call printf                
    addl $8, %esp              

    pushl $format_output_less   
    call printf
    addl $8, %esp

    movl $1, %eax               
    movl $0, %ebx               
    int $0x80                   
