#include <iostream>

using namespace std;

void print_array(string name, int array[], int N)
{
    cout << name << ": ";
    for (int i = 0; i < N; i++)
        cout << array[i] << " ";
    cout << endl;
}

int main()
{
    srand(time(0));

    const int N = 10;

    int array_1[N];
    int array_2[N];
    int array_3[N];

    for (int i = 0; i < N; i++) {

        array_1[i] = rand() % 10;
        array_2[i] = rand() % 10;
    }

    int* ptr_a1 = array_1;
    int* ptr_a2 = array_2;
    int* ptr_a3 = array_3;

    asm(
            "mov %[ptr_a1], %%rsi   \n\t" 
            "mov %[ptr_a2], %%rdi   \n\t" 
            "mov %[ptr_a3], %%rdx   \n\t" 
            "movl %[N], %%ecx       \n\t" 
            
        "main_loop:                 \n\t"
            "movl (%%rsi), %%eax    \n\t" 
            "movl (%%rdi), %%ebx    \n\t" 
            "cmpl %%eax, %%ebx      \n\t" 
            "jg write               \n\t" 
            "movl %%ebx, %%eax      \n\t" 
        
        "write:                     \n\t" 
            "movl %%eax, (%%rdx)    \n\t" 
            "add $4, %%rsi          \n\t" 
            "add $4, %%rdi          \n\t" 
            "add $4, %%rdx          \n\t"
            "loop main_loop         \n\t" 
            :

            :[ptr_a1]"m"(ptr_a1), [ptr_a2]"m"(ptr_a2), [ptr_a3]"m"(ptr_a3), [N]"m"(N)
            :"%rax", "%rbx", "%rcx", "%rdx", "%rsi", "%rdi"
       );

    print_array("Array 1", array_1, N);
    print_array("Array 2", array_2, N);
    
    for (int i = 0; i < 30; i++)
        cout << "+";
    cout << endl;

    print_array("Array 3", array_3, N);

    return 0;
}
