#!/bin/bash

echo "Скрипт сделал Данил Хамалетдинов из 728-2."
echo "Переконвентирует файл в новый формат."

pwd

while true; do

	while true; do
	read -p "Введите имя файла : " file
	if [ -e "$file" ]
	then
	echo "Введите имя копии файла."
	read newfile
	echo "Введите размер нового изображения в процентах или в пикселях x пикселях (50%, 150%, 64x64 и т. д.)."
	read size
	convert $file $newfile
	convert $newfile -resize $size $newfile
	echo "Изображение переконвертировано и созданна его копия c новым размером."
	break
	else
	echo "Такого файла нет."
	echo "Хотите ввести опять?(y/n)"
	read yn
		if [ $yn = "y" ]
		then 
		continue
		else
		break
		fi
	fi
	done	
		
	echo "Хотите продолжить? (y/n)"
	read end_y
        if [ $end_y = "y" ]
        then continue
        else break
        fi
	
done